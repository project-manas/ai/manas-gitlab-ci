#!/bin/bash
set -e

# Install common packages
#-------------------------------------------------------------------------------
apt-get update
apt-get install -qq gcc g++ git python python3

# Enable global C++11 if required by the user
#-------------------------------------------------------------------------------
if [[ ! -z "${GLOBAL_C11}" ]]; then
  echo "Enabling C++11 globally"
  export CXXFLAGS="$CXXFLAGS -std=c++11"
fi

# Display system information
#-------------------------------------------------------------------------------
echo "##############################################"
uname -a
gcc --version
g++ --version
echo "CXXFLAGS = $CXXFLAGS"
python --version
python3 --version
echo "##############################################"


# Install specified packages
#-------------------------------------------------------------------------------
apt-get install -qq $SYSTEM_DEPENDENCIES_TO_INSTALL

