#!/bin/bash
set -e

# Find all C/C++ files and run clang-format on them
#-------------------------------------------------------------------------------
echo "Running clang-format on all C/C++ files"
find . -name '*.h' -or -name '*.hpp' -or -name '*.hh'-or -name '*.hxx' -or -name '*.c'-or -name '*.cpp'-or -name '*.cc'-or -name '*.cxx' | xargs clang-format-3.9 -i -style=Google

# Show changes in formatted code
echo "Showing changes in expected code style:"
git --no-pager diff

# Check if there have been any changes in the repo
#-------------------------------------------------------------------------------
if ! git diff-index --quiet HEAD --; then
    echo "clang-format test failed: changes required to comply to formatting rules. See diff above.";
    exit 1
fi

# Code is formatted correctly
#-------------------------------------------------------------------------------
echo "Passed clang-format test"
exit 0
