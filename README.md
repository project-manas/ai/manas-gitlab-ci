# MANAS GitLab CI
## Description
This repository contains helper scripts and instructions on how to use GitLab Continuous Integration (CI) for MANAS projects hosted on a GitLab.

## How to use
Create a `.gitlab-ci.yml` that looks like this:

```yml
variables:
  ROS_PACKAGES_TO_INSTALL: ""
  SYSTEM_DEPENDENCIES_TO_INSTALL: ""
  GLOBAL_C11: "true"

# Scripts to run before lint stage
.lint_before_script: &lint_before_script
  before_script:
  - git clone https://gitlab.com/shrijitsingh99/manas-gitlab-ci.git
  - source manas-gitlab-ci/install-dependencies.bash

# Scripts to run before ROS build stage
.ros_before_script: &ros_before_script
  before_script:
    - git clone https://gitlab.com/VictorLamoine/ros_gitlab_ci.git
    - source ros_gitlab_ci/gitlab-ci.bash >/dev/null

# CI Stages
stages:
  - build
  - lint

# Cache previous builds
cache:
  paths:
    - ccache/

# yapf
yapf:
  stage: lint
  image: projectmanas/yapf:latest
  <<: *lint_before_script
  script:
    - ./manas-gitlab-ci/yapf.bash

# clang-format
clang-format:
  stage: lint
  image: projectmanas/clang-format:3.9
  <<: *lint_before_script
  script:
    - ./manas-gitlab-ci/clang-format.bash

# catkin_lint
catkin-lint:
  stage: lint
  image: ros:melodic-ros-core
  <<: *ros_before_script
  before_script:
    - apt update  >/dev/null 2>&1
    - apt install -y python-catkin-lint >/dev/null 2>&1
  script:
    - catkin_lint -W3 .

# Tests for Melodic
melodic-catkin_make:
  image: ros:melodic-ros-core
  stage: build
  <<: *ros_before_script
  script:
    - catkin_make


```
Commit and push to your repository and the pipeline will run automatically (make sure pipelines are enabled in your project settings).

## Useful variables
- `SYSTEM_DEPENDENCIES_TO_INSTALL` (empty by default) specify extra system dependencies to install, just add the name of the package to the list, example `libboost-all-dev`
- `ROS_PACKAGES_TO_INSTALL` (empty by default) specify extra ROS packages to install, for `ros-kinetic-rviz` just add `rviz` to the list, the ROS distro is automatically detected.
- `GLOBAL_C11` (not defined by default) forces C++11 for every project compiled, defined it to any value (eg `true`) to globally enable C++11.
